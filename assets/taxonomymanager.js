Bolt.taxonomymanager = {
    
    init: function() {
        var controller = this;

        $("#form_option_add").on('click', function(){
            controller.addoption();
        });

        $('#form_options').on('click', '.taxonomy-image', function(e){
            controller.addimage($(this));
        });

        $( document ).ajaxComplete(function( event, xhr, settings ) {
            if ( settings.url.indexOf(Bolt.conf('paths.async') + "addstack") > -1 ) {
                controller.addUploadedImage(settings.url);
            }
        });

        $("#form_options").sortable();
        $("#form_options").on( "sortupdate", function( event, ui ) {
            controller.orderOptions();
        });

        $('#form_options input.choices-row').select2({tags: true, tokenSeparators: [',']});
        
        $('#form_options').on('click', '.form-row-delete', function(e){ 
            controller.deleteoption($(this));      
            e.preventDefault();
        });
        
        $("#sidebarformdelete").on('submit', function(e){
            if(!confirm("Are you sure you want to permanently delete this Taxonomy?") ) {
                e.preventDefault();
            }
        }); 
        
        $('#form_options').on('change', 'select.type-row', function(){
            controller.handleConditionals();
        }); 
        controller.handleConditionals();
        controller.setupImages();
        
    },
    
    addoption: function() {
        var html = $("#form_options").data("prototype");
        var optionCollectionCount = $('#form_options .form-option').length;
        html = html.replace(/__name__/gi, optionCollectionCount++);
        var proto = $(html);
        //proto.find('input').removeAttr('readonly');
        proto.find('input.choices-row').select2({tags: true, tokenSeparators: [',']});
        $("#form_options").append(proto); 
        $("#form_options").sortable();
        this.handleConditionals();
    },

    addimage: function(image) {
        var wrapper = image.closest('.form-option');
        var imageField = wrapper.find('.form-option-row input.image-row');
        imageField.val(image.data('path'));
        wrapper.find('.taxonomy-image').removeClass('selected');
        image.addClass('selected');
    },

    addUploadedImage: function(file) {
        var controller = this;
        var substrIndex = file.indexOf("addstack/");
        var fileName = file.substring(substrIndex + 9);

        $("#form_options .form-option").each(function() {
            controller.addImageToList($(this), fileName);
        });
    },

    orderOptions: function() {
        $('#form_options .form-option').each(function(index, el) {
            var sortInput = $(el).find('.order-row');
            sortInput.val($(el).index());
        });
    },

    deleteoption: function(trigger) {
        var dodelete = confirm("Are you sure you want to remove a option?");

        if (dodelete) {
            var parent = trigger.closest('.form-option');
            var name = parent.find('input.slug-row').val();
            var replace = '<input type="hidden" name="form[options][_delete][]" value="'+name+'">';
            parent.fadeOut('fast', function(){
                parent.replaceWith(replace);
            });

        }
    },
    
    handleConditionals: function() {
        var textoptions = ['text', 'email', 'number', 'url', 'textarea', 'checkbox'];
        var choiceoptions = ['choice', 'radio', 'checkbox-group'];
        $("#form_options select.type-row").each(function(){
            varoptiontype = $(this).find("option:selected").val();
            var parent = $(this).closest('.outer-row');
            if( $.inArray(varoptiontype, choiceoptions) !== -1 ) {
                parent.find('.choices-row-container').show();
            } else if( $.inArray(varoptiontype, textoptions) !== -1 ) {
                parent.find('.choices-row-container').hide();
            } else {
                parent.find('.required-row-container').hide();
                parent.find('.choices-row-container').hide();
            }
        });
    },

    setupImages: function() {
        var controller = this;
        $("#form_options .form-option").each(function() {
            var imagePath = $(this).find('.form-option-row input.image-row').val();
            var image = $(this).find('.taxonomy-image[data-path="' + imagePath + '"]');
            if (image.length === 0) {
                controller.addImageToList($(this), imagePath);
            } else {
                image.addClass('selected');
            }
        });
    },

    addImageToList: function(container, imagePath) {
        var parent = container.find('.taxonomy-images');
        var imageList = parent.find('.taxonomy-image-wrapper');

        if (imageList.length < 10) {
            imageList.last().clone().prependTo(parent);
        } else if (imageList.last().find('.taxonomy-image').hasClass('selected')) {
            imageList.last().prev().prependTo(parent);
        } else {
            imageList.last().prependTo(parent);
        }

        var imageWrapper = parent.find('.taxonomy-image-wrapper').first();
        var image = imageWrapper.find('.taxonomy-image');
        var src = Bolt.conf('paths.bolt') + '../thumbs/100x100c/' + encodeURI(imagePath);
        image.removeClass('selected');
        image.data('path', imagePath);
        image.attr('src', src);
    }
    
}

jQuery(document).ready(function($) {
    Bolt.taxonomymanager.init();

    $('.ct-taxonomies').select2({
        allowClear: true,
        minimumResultsForSearch: 10
    });
});