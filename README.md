Taxonomy Manager
================

With this extension you can manage taxonomies from backend. You can not only create new taxonomies, but edit it's properties and it's options also. Furthermore, you can assign or delete the taxonomies to content types.

### Configuration

No configurations needed!

### Features

The main feature that extension propvides is the posibility to assign an image to each option of a taxonomy. For example, yo can show a gallery of categories with images and links.

Then, you can retrieve the image easily like this:

```
    {% set categories = config.get('taxonomy/categories') %}
    {% set path = paths.root ~ categories.slug %}
    <ul>
    {% for slug, option in categories.options %}
        <li>
            <a href="{{ path ~ '/' ~ slug }}"><img src="{{ categories.images[slug]|thumbnail(100,100) }}"></a>
        </li>
    {% endfor %}
    </ul>
```

