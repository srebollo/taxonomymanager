<?php

namespace Bolt\Extension\Binima\TaxonomyManager\Controller;

use Bolt\Application;
use Bolt\Translation\Translator as Trans;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cocur\Slugify\Slugify;

use Bolt\Extension\Binima\TaxonomyManager\Extension;
use Bolt\Extension\Binima\TaxonomyManager\Form;

class TaxonomyController implements ControllerProviderInterface
{
    /**
     * Bolt Application instance
     *
     * @var Application
     */
    private $app;

    /**
     * Extension configuration
     *
     * @var array
     */
    private $config;

    /**
     * Specify which method handles which route.
     *
     * Base route/path is '/extensions/taxonomy-manager' (see Extension.php)
     *
     * @param \Silex\Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(\Silex\Application $app)
    {
        $this->app = $app;
        $this->config = $app[Extension::CONTAINER]->config;

        /** @var $ctr \Silex\ControllerCollection */
        $ctr = $app['controllers_factory'];

        // /extensions/taxonomy-manager
        $ctr->get('/', array($this, 'index'))
            ->bind('taxonomymanager.index'); // route name, must be unique(!)

        $ctr->post('/', array($this, 'index'))
            ->bind('taxonomymanager.create');

        $ctr->get('/edit/{taxonomyname}', array($this, 'edit'))
            ->bind('taxonomymanager.edit');

        $ctr->post('/edit/{taxonomyname}', array($this, 'edit'))
            ->bind('taxonomymanager.save');

        $ctr->post('/delete/{taxonomyname}', array($this, 'delete'))
            ->bind('taxonomymanager.delete');

        $ctr->post('/savect/{contenttypename}', array($this, 'savect'))
            ->bind('taxonomymanager.savect');

        $ctr->before(array($this, 'before'));

//        $stackItems = $app['stack']->listitems();
//        foreach ($stackItems as $key => $item) {
//            $app['stack']->delete($item['filepath']);
//        }

        return $ctr;
    }

    /**
     * Controller before render
     *
     * @param Request            $request
     * @param \Silex\Application $app
     */
    public function before(Request $request, Application $app)
    {
        // execute only when in backend
        if ($app['config']->getWhichEnd() === 'backend') {
            $app['htmlsnippets'] = true;

            // add CSS and Javascript files to all requests in the backend
            $app[Extension::CONTAINER]->addJavascript('assets/jquery.sortable.min.js', array('late' => true));
            $app[Extension::CONTAINER]->addJavascript('assets/taxonomymanager.js', array('late' => true));
            $app[Extension::CONTAINER]->addCss('assets/taxonomymanager.css');
        }
    }

    /**
     * Index page for extension, lists current forms and provides a
     * form to create a new one.
     *
     * @param Application $app
     * @param Request     $request
     *
     * @return Response
     */
    public function index(Application $app, Request $request)
    {
        $form = $app['form.factory']
            ->createBuilder('form')
            ->add('name', 'text', ['required' => true])
            ->getForm();

        if ($request->getMethod() == 'POST') {
            $dataTaxonomies = $request->request->get($form->getName());
            if ($dataTaxonomies) {
                $this->createTaxonomy($dataTaxonomies);
            } else {
                $this->app['session']->getFlashBag()->set('error', Trans::__('Unable to create the taxonomy'));
            }
        }

        return $app['render']->render('taxonomy_manager_index.twig', [
            'taxonomies' => $this->getData('taxonomy.yml'),
            'contenttypes' => $this->getData('contenttypes.yml'),
            'create' => $form->createView(),
        ]);
    }

    /**
     * Taxonomy edit page.
     *
     * @param Application $app
     * @param Request     $request
     * @param string      $taxonomyname The name of the taxonomy
     *
     * @return Response
     */
    public function edit(Application $app, Request $request, $taxonomyname)
    {
        $form = $this->buildForm($taxonomyname);

        if ($request->getMethod() == 'POST') {
            $data = $request->request->get($form->getName());
            $this->save($data, $taxonomyname);
            $this->app['session']->getFlashBag()->set('success', Trans::__('Taxonomy successfully updated'));
            $form = $this->buildForm($taxonomyname);
        }

        return $app['render']->render('taxonomy_manager_edit.twig', [
            'form' => $form->createView(),
            'taxonomyname' => $taxonomyname
        ]);
    }

    /**
     * POST method that handles the deletion of a taxonomy.
     *
     * @param Application $app
     * @param Request     $request
     * @param string      $taxonomyname name of the taxonomy
     *
     * @return RedirectResponse returns the user to the index page
     */
    public function delete(Application $app, Request $request, $taxonomyname)
    {
        $data = $this->readFile('taxonomy.yml');
        if (array_key_exists($taxonomyname, $data)) {
            unset($data[$taxonomyname]);
            $response = $this->write($data, 'taxonomy.yml');
            if ($response) {
                $this->app['session']->getFlashBag()->set('success', Trans::__('Taxonomy successfully deleted'));
            }
        }

        return new RedirectResponse($this->app['url_generator']->generate('taxonomymanager.index'));
    }

    /**
     * Internal method that initialises a new taxonomy with some defaults.
     *
     * @param array $data data from the create form
     */
    protected function createTaxonomy($data)
    {
        $slugify = new Slugify();

        $newname = $data['name'];
        $cleanname = $slugify->slugify($newname, '_');

        $existing = $this->getData('taxonomy.yml', $cleanname);

        if ($existing) {
            $this->app['session']->getFlashBag()->set('error', Trans::__('The taxonomy name you choose already exists'));

            return;
        }

        $fulldata = $this->readFile('taxonomy.yml');
        $fulldata[$cleanname] = ['name' => $newname, 'slug' => $cleanname];

        if ($this->write($fulldata, 'taxonomy.yml')) {
            $this->app['session']->getFlashBag()->set('success', Trans::__('Your new taxonomy has been created'));
        }
    }

    /**
     * Internal method that builds a Symfony Form from the name supplied.
     *
     * @param string $taxonomyname
     *
     * @return Form
     */
    protected function buildForm($taxonomyname)
    {
        $formdata = $this->getData('taxonomy.yml', $taxonomyname);
        $formdata = $this->formatTaxonomyData($formdata);

        $form = $this->app['form.factory']
            ->createBuilder('form')
            ->add('settings', 'collection', [
                'type' => new Form\SettingsType(),
                'data' => [$formdata['settings']]
            ])
            ->add('options', 'collection',   [
                'type' => new Form\OptionType(),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'prototype_name' => '__name__',
                'data' => $formdata['options'],
            ])
            ->getForm();

        return $form;
    }

    /**
     * Internal method that takes an array of posted data which
     * is in a slightly simplified structure and maps it correctly
     * to the Taxonomy YML layout.
     *
     * @param array  $newdata  Posted data from the form
     * @param string $taxonomyname
     *
     * @return bool Whether the save was succesful
     */
    protected function save($newdata, $taxonomyname)
    {
        $fulldata = $this->readFile('taxonomy.yml');
        $slugify = new Slugify();

        if (isset($newdata['settings'])) {
            foreach ($newdata['settings'][0] as $key => $value) {
                if (in_array($key, ['has_sortorder', 'allow_spaces', 'multiple_check'])) {
                    $fulldata[$taxonomyname][$key] = ($value === '1') ? true : false;
                } else if (!empty($value)) {
                    $fulldata[$taxonomyname][$key] = is_numeric($value) ? intval($value) : $value;
                } else {
                    unset($fulldata[$taxonomyname][$key]);
                }
            }
        }

        if (!empty($fulldata[$taxonomyname]['multiple_check']) &&
            $fulldata[$taxonomyname]['multiple_check'] &&
            !empty($fulldata[$taxonomyname]['multiple_number'])) {
            $fulldata[$taxonomyname]['multiple'] = $fulldata[$taxonomyname]['multiple_number'];
            unset($fulldata[$taxonomyname]['multiple_check']);
            unset($fulldata[$taxonomyname]['multiple_number']);
        }

        if (!empty($fulldata[$taxonomyname]['name'])) {
            $fulldata[$taxonomyname]['slug'] = $slugify->slugify($fulldata[$taxonomyname]['name'], '_');
        }

        if (!empty($fulldata[$taxonomyname]['singular_name'])) {
            $fulldata[$taxonomyname]['singular_slug'] = $slugify->slugify($fulldata[$taxonomyname]['singular_name'], '_');
        }

        if (isset($newdata['options']['_delete'])) {
            foreach ($newdata['options']['_delete'] as $option) {
                if (array_key_exists($option, $fulldata[$taxonomyname]['options'])) {
                    unset($fulldata[$taxonomyname]['options'][$option]);
                    unset($fulldata[$taxonomyname]['images'][$option]);
                }
            }
            unset($newdata['options']['_delete']);
        }

        if (!empty($newdata['options'])) {
            $fulldata[$taxonomyname]['options'] = [];
            $fulldata[$taxonomyname]['images'] = [];
            foreach ($newdata['options'] as $name => $values) {
                $key = $values['slug'];
                $fulldata[$taxonomyname]['options'][$key] = $values['name'];
                $fulldata[$taxonomyname]['images'][$key] = $values['image'];
            }
        } else {
            unset($fulldata[$taxonomyname]['options']);
            unset($fulldata[$taxonomyname]['images']);
        }

        return $this->write($fulldata, 'taxonomy.yml');
    }

    /**
     * POST method that handles the save of the taxonomies of a content type.
     *
     * @param Application $app
     * @param Request     $request
     * @param string      $contenttypename name of the content type
     *
     * @return RedirectResponse returns the user to the index page
     */
    public function savect(Application $app, Request $request, $contenttypename)
    {
        $newData = $request->request->all();

        $data = $this->getData('contenttypes.yml');
        if (array_key_exists($contenttypename, $data)) {
            $data[$contenttypename]['taxonomy'] = $newData['ct-taxonomies-' . $contenttypename];
            $response = $this->write($data, 'contenttypes.yml');
            if ($response) {
                $this->app['session']->getFlashBag()->set('success', Trans::__('Content Type Taxonomies successfully updated'));
            }
        }

        return new RedirectResponse($this->app['url_generator']->generate('taxonomymanager.index'));
    }

    /**
     * Format data for form.
     *
     * @param  array $data
     * @return array
     */
    protected function formatTaxonomyData($data)
    {
        $newData = [];

        foreach ($data as $name => $value) {
            if ($name === 'options') {
                foreach ($data['options'] as $slug => $option) {
                    $image = $data['images'][$slug];
                    $newData['options'][] = ['name' => $option, 'slug' => $slug, 'image' => $image];
                }
            } else if ($name !== 'images') {
                if ($name === 'multiple') {
                    $newData['settings']['multiple_check'] = (bool)$value;
                    if (is_numeric($value)) {
                        $newData['settings']['multiple_number'] = $value;
                    }
                } else {
                    $newData['settings'][$name] = $value;
                }
            }
        }

        return $newData;
    }

//    /**
//     * Handles reading the Taxonomies yml file.
//     *
//     * @return array The parsed data
//     */
//    protected function readTaxonomies()
//    {
//        //config parse Taxonomy
//        $file = $this->getFile('taxonomy.yml');
//        $yaml = file_get_contents($file);
//        $parser = new Parser();
//        $data = $parser->parse($yaml);
//
//        return $data;
//    }

    /**
     * Handles reading the yml file.
     *
     * @param string $fileName
     *
     * @return array The parsed data
     */
    protected function readFile($fileName = 'taxonomy.yml')
    {
        $file = $this->getFile($fileName);
        $yaml = file_get_contents($file);
        $parser = new Parser();
        $data = $parser->parse($yaml);

        return $data;
    }

//    /**
//     * Reads the raw data, unsets the non-form settings and returns.
//     * If passed a parameter then only the single form data will be returned.
//     *
//     * @param string|null $form
//     *
//     * @return array
//     */
//    protected function getTaxonomies($taxonomy = false)
//    {
//        $data = $this->readTaxonomies();
//
//        if ($taxonomy && array_key_exists($taxonomy, $data)) {
//            return $data[$taxonomy];
//        } elseif ($taxonomy) {
//            return false;
//        }
//
//        return $data;
//    }

    /**
     * Reads the raw data, unsets the non-form settings and returns.
     * If passed a parameter then only the single form data will be returned.
     *
     * @param string|null $fileName
     * @param string|null $element
     *
     * @return array
     */
    protected function getData($fileName = 'taxonomy.yml', $element = false)
    {
        $data = $this->readFile($fileName);

        if ($element && array_key_exists($element, $data)) {
            return $data[$element];
        } elseif ($element) {
            return false;
        }

        return $data;
    }

    /**
     * Internal method that handles writing the data array back to the YML file.
     *
     * @param array $data
     * @param string $fileName
     *
     * @return bool True if successful
     */
    protected function write($data, $fileName = 'taxonomy.yml')
    {
        $dumper = new Dumper();
        $dumper->setIndentation(4);
        $yaml = $dumper->dump($data, 9999);
        $file = $this->getFile($fileName);
        try {
            $response = @file_put_contents($file, $yaml);
        } catch (\Exception $e) {
            $response = null;
        }

        return $response;
    }

//    /**
//     * Handles reading the Taxonomies yml file.
//     *
//     * @return string the file name
//     */
//    protected function getTaxonomiesFile()
//    {
//        $configDirectory = $this->app['resources']->getPath('config');
//
//        /**
//         * check if taxonomy.yml is writable
//         */
//        $file = $configDirectory . '/taxonomy.yml';
//        if (@!is_readable($file) || !@is_writable($file)) {
//            throw new \Exception(
//                Trans::__("The file '%s' is not writable. You will have to use your own editor to make modifications to this file.",
//                    array('%s' => $file)));
//        }
//
//        return $file;
//    }

    /**
     * Handles reading the yml file.
     *
     * @param string|null $fileName
     *
     * @return string the file name
     */
    protected function getFile($fileName = 'taxonomy.yml')
    {
        $configDirectory = $this->app['resources']->getPath('config');

        /**
         * check if file is writable
         */
        $file = $configDirectory . '/' . $fileName;
        if (@!is_readable($file) || !@is_writable($file)) {
            throw new \Exception(
                Trans::__("The file '%s' is not writable. You will have to use your own editor to make modifications to this file.",
                    array('%s' => $file)));
        }

        return $file;
    }
}
