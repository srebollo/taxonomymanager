<?php

namespace Bolt\Extension\Binima\TaxonomyManager;

use Bolt\Application;
use Bolt\BaseExtension;
use Bolt\Extension\Binima\TaxonomyManager\Controller\TaxonomyController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Bolt\Translation\Translator as Trans;

class Extension extends BaseExtension
{
    const CONTAINER = 'extensions.taxonomymanager';

    /**
     * Constructor adds an additional Twig path if we are in the Backend.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        if ($this->app['config']->getWhichEnd() == 'backend') {
            $this->app['twig.loader.filesystem']->addPath(__DIR__.'/twig');
        }
    }

    /**
     * The extension name
     *
     * @return string
     */
    public function getName()
    {
        return 'taxonomymanager';
    }

    public function initialize()
    {
        $path = $this->app['config']->get('general/branding/path') . '/extensions/taxonomy-manager';
        $menuPath = $this->app['resources']->getUrl('bolt') . 'extensions/taxonomy-manager';

        if ($this->checkAuth()) {
            $this->app->mount($path, new TaxonomyController());
            $this->addMenuOption(Trans::__('Taxonomy Manager'), $menuPath, 'fa:tags');
        }
    }

    /**
     * Checks that the user has a non-guest role.
     *
     * @return bool
     */
    public function checkAuth()
    {
        $currentUser = $this->app['users']->getCurrentUser();
        $currentUserId = $currentUser['id'];

        foreach (['admin', 'root', 'developer', 'editor'] as $role) {
            if ($this->app['users']->hasRole($currentUserId, $role)) {
                return true;
            }
        }

        return false;
    }
}
