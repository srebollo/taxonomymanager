<?php

namespace Bolt\Extension\Binima\TaxonomyManager\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Bolt\Translation\Translator as Trans;

/**
 * A Form Collection that sets up the options for the repeating field types.
 */
class OptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',   'text', [
                'label' => Trans::__('Name'),
//                'attr' => ['help' => 'Only letters, numbers and underscores allowed'],
            ])
            ->add('slug',   'text', [
                'label' => Trans::__('Slug'),
                'attr' => ['help' => Trans::__('Only letters, numbers and underscores allowed')],
            ])
            ->add('image',   'text', [
                'label' => Trans::__('Image'),
                'read_only' => true,
                'attr' => ['help' => Trans::__('Image associated to this taxonomy option')],
            ])
            ->add('order',   'hidden', [
                'attr' => ['class' => 'order-row'],
            ])
        ;
    }

    public function getName()
    {
        return 'option';
    }
}
