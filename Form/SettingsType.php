<?php

namespace Bolt\Extension\Binima\TaxonomyManager\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Bolt\Translation\Translator as Trans;

/**
 * A form collection that provides options for notification settings.
 */
class SettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',   'text', [
                'label' => Trans::__('Name'),
//                'attr' => ['help' => 'Only letters, numbers and underscores allowed'],
            ])
            ->add('slug',   'text', [
                'label' => Trans::__('Slug'),
                'read_only' => true,
                'attr' => ['help' => Trans::__('Generated automatically from name')],
            ])
            ->add('singular_name',   'text', [
                'label' => Trans::__('Singular name'),
//                'attr' => ['help' => 'Generated automatically from name'],
            ])
            ->add('singular_slug',  'text', [
                'label' => Trans::__('Singular slug'),
                'read_only' => true,
                'attr' => ['help' => Trans::__('Generated automatically from singular name')],
            ])
            ->add('behaves_like',  'choice', [
                'label' => Trans::__('Taxonomy Type'),
                'choices' => [
                    'tags' => Trans::__('Tags'),
                    'categories' => Trans::__('Categories'),
                    'grouping' => Trans::__('Grouping'),
                ],
            ])
            ->add('has_sortorder', 'checkbox', [
                'label' => Trans::__('Has sort order'),
                'required' => false,
            ])
            ->add('allow_spaces', 'checkbox', [
                'label' => Trans::__('Allow spaces'),
                'required' => false,
                'attr' => ['help' => Trans::__('Check if you want to allow spaces between tags (only for tags taxonomy type)')],
            ])
            ->add('postfix', 'text', [
                'label' => Trans::__('Text for help in selecting taxonomy'),
                'required' => false,
            ])
            ->add('multiple_check', 'checkbox', [
                'label' => Trans::__('Multiple options for this taxonomy can be selected'),
                'required' => false,
            ])
            ->add('multiple_number', 'integer', [
                'label' => Trans::__('Number of multiple options'),
                'required' => false,
                'attr' => ['help' => Trans::__('If you select multiple options, you can choice the number of them')],
            ])
        ;
    }

    public function getName()
    {
        return 'settings';
    }
}
